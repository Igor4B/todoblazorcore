﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ToDoListBlazorTwo.Model
{
    [Table("todoitem")]
    public class ToDoItem
    {
        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column("name")]
        [Required]
        public string Name { get; set; }

        [Column("done")]
        public bool Done { get; set; }

        [Column("tasklistid")]
        public int TaskListId { get; set; }
        [ForeignKey("TaskListId")]
        public TaskList TaskList { get; set; }


    }
}