﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToDoListBlazorTwo.Model;

namespace ToDoListBlazorTwo.Data
{
    public class ToDoBlazorTwoContext : DbContext
    {
        public ToDoBlazorTwoContext(DbContextOptions<ToDoBlazorTwoContext> options) : base(options) 
        {
        }
        public DbSet<ToDoItem> ToDoItem { get; set; }
        public DbSet<TaskList> TaskList { get; set; }
    }
}
